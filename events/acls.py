from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json
import requests


def get_photo(city, state):
    params = {"query": f"{city}, {state}", "per_page": 1}
    url = 'https://api.pexels.com/v1/search'
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return{"picture_url": None}

    
def get_weather_data(city, state):
    response = requests.get(OPEN_WEATHER_API_KEY)
    clue = json.loads(response.content)
    get_weather_data = {
        "city": clue["city"],
        "state": clue["state"],
    }
    return get_weather_data
